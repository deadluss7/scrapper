'use strict';

var Enviroment = require('./Enviroment');

class TaxasInvestimentos {
	constructor() {
		this.host = Enviroment.host;
		this.api = "/api/tx/verificaTaxa";
	}
	
	getTaxas(data) {
		// return new pending promise
		var _data = JSON.stringify(data);
		var options = {
			hostname: Enviroment.hostname,
			path: this.api,
			method: 'POST',
			headers: {"Content-Type": "application/json"}
		};
		return new Promise((resolve, reject) => {
			const lib = Enviroment.protocolLib;
			const request = lib.request(options, (response) => {
				// handle http errors
				if (response.statusCode == 500 || response.statusCode == 503) {
					reject(new Error(response.statusCode + ": Fatal ERROR!"));
				}
				// temporary data holder
				const body = [];
				// on every content chunk, push it to the data array
				response.on('data', (chunk) => body.push(chunk));
				// we are done, resolve promise with those joined chunks
				response.on('end', () => {
					try {
						var _body = body.join('');
						// handle http errors
						if (response.statusCode !== 200) {
							reject(new Error(response.statusCode + ": " + _body));
						}
						resolve(JSON.parse(_body));
					} catch (err) {
						reject(err);
					}
				});
			});
			// handle connection errors of the request
			request.on('error', (err) => reject(err));
			request.write(_data);
			request.end();
		});
	}
}

module.exports = new TaxasInvestimentos();

/*
//  -*- coding: utf-8 -*-
// import
// from util.working_days import *
from decimal import *
import datetime
from dateutil import rrule
import dateutil
from RendaFixaUtils.connection_factory import connection_factory


function calcula_equivalecia(tipo, ir, taxa_ano) {
    if tipo in ["LCA", "LCI"]:
        return taxa_ano / (1 - ir)
    else:
        return taxa_ano * (1 - ir)
}


class TaxasInvestimentos {
    //	classe responsavel por calcular taxas de investimento //

    // __init__(self) {
        //  db = connection_factory().connect()
        //  taxas = db.taxas.find_one()
        //  self.ipca12 = Decimal(taxas["ipca_acu_12"])
        //  self.cdi = Decimal(taxas["cdi"])
        //  TaxasInvestimentos.selic = Decimal(taxas["selic"])
        //  self.igpm = Decimal(taxas['igpm_acu_12'])
        //  TaxasInvestimentos.TWOPLACES = Decimal(10) ** -2
        // 
        //  self.taxas_100_porcento = {
        //      'cdi': '100% CDI',
        //      'ipc-a ':'100% IPC-A',
        //      'selic' :'100% selic',
        //      'igpm'  :'IGPM + 100%'
        //  }

    ipca12 = 0
    cdi = 0
    selic = 0
    igpm = 0
    igpdi12 = 0
    TWOPLACES = Decimal(10) ** -2
    taxas_100_porcento = {
        'CDI': '100% CDI',
        'IPCA':'100% IPCA',
        'SELIC' :'100% SELIC',
        'IGPM'  :'100% IGPM',
        'IGPDI'  :'100% IGPDI'
    }

    @staticmethod
    get_taxes() {
        db = connection_factory().connect()
        taxas = db.taxas.find_one()

        TaxasInvestimentos.ipca12 = float(taxas["ipca_acu_12"])
        TaxasInvestimentos.cdi = float(taxas["cdi"])
        TaxasInvestimentos.selic = float(taxas["selic"])
        TaxasInvestimentos.igpm = float(taxas['igpm_acu_12'])
        TaxasInvestimentos.igpdi12 = float(taxas['igpdi_acu_12'])
        TaxasInvestimentos.poup12 = float(taxas['poup_acu_12'])
        TaxasInvestimentos.TWOPLACES = Decimal(10) ** -2

        TaxasInvestimentos.taxas_100_porcento = {
            'CDI': '100% CDI',
            'IPCA':'100% IPCA',
            'SELIC' :'100% SELIC',
            'IGPM'  :'100% IGPM',
            'IGPDI'  :'100% IGPDI'
        }
	}

    to_decimal(self, valor) {
        total = Decimal(valor).quantize(TaxasInvestimentos.TWOPLACES)
        return total
	}

    get_working_days(self,date_start_obj, date_end_obj) {
        ano_novo = dateutil.rrule.rrule(dateutil.rrule.YEARLY, dtstart=date_start_obj, until=date_end_obj, bymonth=1, bymonthday=1)              //  Ano Novo
        sexta_santa = dateutil.rrule.rrule(dateutil.rrule.YEARLY, dtstart=date_start_obj, until=date_end_obj, bymonth=3, bymonthday=3)              //  Sexta-feira Santa
        tiradentes = dateutil.rrule.rrule(dateutil.rrule.YEARLY, dtstart=date_start_obj, until=date_end_obj, bymonth=3, bymonthday=21)             //  Tiradentes
        trabalho = dateutil.rrule.rrule(dateutil.rrule.YEARLY, dtstart=date_start_obj, until=date_end_obj, bymonth=5, bymonthday=1)              //  Dia do Trabalho
        indep = dateutil.rrule.rrule(dateutil.rrule.YEARLY, dtstart=date_start_obj, until=date_end_obj, bymonth=9, bymonthday=7)              //  Independencia
        aparecida = dateutil.rrule.rrule(dateutil.rrule.YEARLY, dtstart=date_start_obj, until=date_end_obj, bymonth=10, bymonthday=12)            //  Nossa Senhora Aparecida
        finados = dateutil.rrule.rrule(dateutil.rrule.YEARLY, dtstart=date_start_obj, until=date_end_obj, bymonth=11, bymonthday=2)             //  Finados
        proc_rep = dateutil.rrule.rrule(dateutil.rrule.YEARLY, dtstart=date_start_obj, until=date_end_obj, bymonth=11, bymonthday=15)            //  Proclamacao Republica
        natal = dateutil.rrule.rrule(dateutil.rrule.YEARLY, dtstart=date_start_obj, until=date_end_obj, bymonth=12, bymonthday=25)            //  Natal

        feriados = len(list(ano_novo)) +  + len(list(sexta_santa)) + len(list(tiradentes)) + len(list(trabalho)) + len(list(indep)) + len(list(aparecida)) + len(list(finados)) + len(list(proc_rep)) + len(list(natal))
        weekdays = dateutil.rrule.rrule(dateutil.rrule.DAILY, byweekday=range(0, 5), dtstart=date_start_obj, until=date_end_obj)
        return len(list(weekdays)) - feriados
	}

    get_ir(self, days) {
        if days <= 180:
            return 0.225
        elif days <= 360:
            return 0.20
        elif days <= 720:
            return 0.175
        else:
            return 0.15
	}

    get_days_from_year(self, qtd_ano) {
        days_base= 365
        return days_base * qtd_ano
	}

    get_days_from_month(self, count_month) {
        month_base = 30
        return count_month * month_base
	}

    int_to_date(self, dias) {
        now = datetime.date.today()
        date = now + datetime.timedelta(days=dias)
        return date
	}

    string_days_to_int(self, date) {
        formato = '%d/%m/%Y'
        now = datetime.date.today()
        vencimento = datetime.date.today()
        try: vencimento = datetime.datetime.strptime(str(date), formato).date()
        except: vencimento = datetime.datetime.strptime(str(date),  '%d/%m/%y').date()
        data = vencimento - now
        return data.days
	}

    // CALCULOS ITENS
    calcular_taxa_periodo(self, taxa_ano, dias_uteis) {
       taxa_periodo = (taxa_ano ** Decimal(dias_uteis / 252)) // 252 = dias correntes no ano
       return taxa_periodo
	}

    get_dias_uteis(self, vencimento) {
        currentDate = datetime.datetime.today().date()
        vencimento = self.int_to_date(vencimento)
        dias_uteis = self.get_working_days(currentDate, vencimento)
        return dias_uteis
	}

    get_dias_corridos(self, dias_corridos) {
        if type(dias_corridos) == str and '/' in dias_corridos  :
                dias_corridos  = self.string_days_to_int(dias_corridos)
        elif type(dias_corridos) == str and ('dias' in dias_corridos.lower() or 'dia' in dias_corridos.lower()) {
                dias_corridos = int(dias_corridos.lower().replace(' ', '').replace('dias', '').replace('dia', ''))

        elif type(dias_corridos) == str and 'ano' in dias_corridos.lower() {
            dias_corridos = dias_corridos.lower()
            dias_corridos = self.get_days_from_year(int(dias_corridos.replace(' ano', '').replace('s', '')))

        elif type(dias_corridos) == str and 'meses' in dias_corridos.lower() {
            dias_corridos = self.get_days_from_month(int(dias_corridos.replace(' meses', '')))

        elif type(dias_corridos) == str and 'mes' in dias_corridos.lower() {
            dias_corridos = self.get_days_from_month(int(dias_corridos.replace('mes', '')))

        else:
            dias_corridos= int(dias_corridos)

        return dias_corridos
	}

    get_days(self, dias) {
        dias_corridos = self.get_dias_corridos(dias)
        dias_uteis = self.get_dias_uteis(dias_corridos)
        return dias_corridos, dias_uteis
	}


    get_ir(self, days) {
        if days <= 180:
            return 0.225
        elif days <= 360:
            return 0.20
        elif days <= 720:
            return 0.175
        else:
            return 0.15
	}


    verifica_taxa(self, taxa, valor_minimo, dias, tipo) {
        rendimentos_isentos_de_ir = ['LCA', 'LCI', 'CRA', 'CRI']

        dias_corridos, dias_uteis= self.get_days(dias)
        if(tipo in rendimentos_isentos_de_ir) {
            ir = 0
        else:
            ir = float(self.get_ir(dias_corridos)) //  IR eh sempre em dias corridos. O rendimento eh em dias uteis!! =)


        // teste apenas. Apagar depois.
        // TODO: Criar um MOC para testes
        // taxa = "CDI"
        // taxa = "CDI - 2,5%"
        // taxa = "CDI + 2,5%"
        // taxa = "CDI + 2,5%"
        // taxa = "IPCA + 2,5%"
        // taxa = "IPCA 2,5%"
        // taxa = "IPCA - 2,5%"
        // taxa = "IPCA"
        // taxa = "16,5% PRE"
        // taxa = "IGPM 2,5%"
        // taxa = "IGPM +2,5%"
        // taxa = "7% IGP-DI"
        // taxa = "+14,5%"

        // verifica se nao e uma taxa 100%
        taxas = TaxasInvestimentos.taxas_100_porcento
        taxa = taxa.upper().replace('IPC-A', 'IPCA').replace('IGP-DI', 'IGPDI').replace('IGP-M', 'IGPM').replace('PREFIXADO', 'PRE').replace('PRE', 'PRE').replace('AA', '').replace('A.A', '').replace(',', '.')
        if taxa in taxas.keys() {
            taxa = TaxasInvestimentos.taxas_100_porcento[taxa]

        // MISTO - Trato como se fosse Pre, pois eu pego a taxa CDI dia e somo com a taxa pre
        if 'CDI -' in taxa:
            // CDI -1,08%
            taxa = float(taxa.replace('CDI ', '').replace('%', '').replace('-', '').replace(' ', '').replace(',', '.'))
            taxa_cdi = float(1 + (TaxasInvestimentos.cdi / 100)) / (1 + taxa/100)
            taxa_total = (taxa_cdi - 1) * 100
            rentabilidade_bruta_dia = (taxa_cdi ** (1/252) - 1) * 100
            taxaAntiga = "CDI -{0}%".format(taxa)
            indice = 'CDI'

        // MISTO - Trato como se fosse Pre, pois eu pego a taxa CDI dia e somo com a taxa pre
        elif 'CDI +' in taxa:
            // CDI +1,08%
            taxa = float(taxa.replace('CDI ', '').replace('%', '').replace('+', '').replace(' ', '').replace(',', '.'))
            taxa_cdi = float(1 + (TaxasInvestimentos.cdi / 100)) * (1 + taxa/100)
            taxa_total = (taxa_cdi - 1) * 100
            rentabilidade_bruta_dia = (taxa_cdi ** (1/252) - 1) * 100
            taxaAntiga = "CDI -{0}%".format(taxa)
            indice = 'CDI'

        // POS
        elif 'CDI' in taxa:
            // 108% CDI
            taxa = float(taxa.replace('%', '').replace(' DO CDI', '').replace('DO', '').replace('do', '').replace('CDI', '').replace(' ', '').replace(',', '.'))
            taxa_cdi = (float(1 + (TaxasInvestimentos.cdi / 100)) ** float(1/252) - 1)  // 
            taxa_cdi_dia = taxa_cdi * (taxa/100)
            taxa_total = ((1 + taxa_cdi_dia) ** 252 - 1) * 100
            rentabilidade_bruta_dia = taxa_cdi_dia * 100 //  (1.1163 ** (0.003968) - 1) * 100 = 0.043668
            taxaAntiga = "{0}% CDI".format(taxa)
            indice = 'CDI'

        // POS
        elif 'IPCA' in taxa:
            taxaAntiga = taxa
            taxa = float(taxa.replace('IPCA', '').replace('%', '').replace('+', '').replace('-', '').replace(' ', '').replace(',', '.'))
            indice = 'IPCA'

            if '+' in taxaAntiga:
                //  7,39% + IPCA
                taxa_ipca = float(1 + (TaxasInvestimentos.ipca12 / 100)) * (1 + taxa/100)
                taxa_total = (taxa_ipca - 1) * 100
                rentabilidade_bruta_dia = (taxa_ipca ** (1/252) - 1) * 100//  (1.1313 ** (0.003968) - 1) * 100 = 0,0490
                taxaAntiga = "IPCA +{0}%".format(taxa)
            elif '-' in taxaAntiga:
                taxa_ipca = float(1 + (TaxasInvestimentos.ipca12 / 100)) / (1 + taxa/100)
                taxa_total = (taxa_ipca - 1) * 100
                rentabilidade_bruta_dia = (taxa_ipca ** (1/252) - 1) * 100
                taxaAntiga = "IPCA -{0}%".format(taxa)
            else:
                if taxa > 30:
                    //  IPCA
                    taxa_ipca = (float(1 + (TaxasInvestimentos.ipca12 / 100)) ** float(1/252) - 1)  // 
                    taxa_ipca_dia = taxa_ipca * (taxa/100)
                    taxa_total = ((1 + taxa_ipca_dia) ** 252 - 1) * 100
                    rentabilidade_bruta_dia = taxa_ipca_dia * 100//  (1.1313 ** (0.003968) - 1) * 100 = 0,0490
                    taxaAntiga = "{0}% IPCA".format(taxa)

                else:
                    //  IPCA 2,5%
                    taxa_ipca = float(1 + (TaxasInvestimentos.ipca12 / 100)) * (1 + taxa/100)
                    taxa_total = (taxa_ipca - 1) * 100
                    rentabilidade_bruta_dia = (taxa_ipca ** (1/252) - 1) * 100//  (1.1313 ** (0.003968) - 1) * 100 = 0,0490
                    taxaAntiga = "IPCA +{0}%".format(taxa)

        // PRE
        elif 'PRE' in taxa:
            //  14.5%
            taxa = float(taxa.replace(' ', '').replace('%', '').replace('-', '').replace('PRE', '').replace('A.A', ''))
            taxa_total = taxa
            fator_taxa = float(1 + (taxa / 100)) //  14.5 = 1.145
            rentabilidade_bruta_dia = (fator_taxa ** float(1/252) - 1) * 100
            taxaAntiga = "{0}%".format(taxa)
            indice = 'PRE'

        // MISTO
        elif 'IGPM' in taxa:
            // IGPM + 3.4%
            taxa = float(taxa.replace('IGPM', '').replace('+', '').replace('%', '').replace(',', '.'))
            taxa_igpm = float(1 + (TaxasInvestimentos.igpm / 100)) * (1 + taxa/100)
            taxa_total = (taxa_igpm - 1) * 100
            rentabilidade_bruta_dia = (taxa_igpm ** (1/252) - 1) * 100
            taxaAntiga = "IGPM +{0}%".format(taxa)
            indice = 'IGPM'

        elif 'IGPDI' in  taxa:
            // 7% IGP-DI
            taxa = float(taxa.replace('IGPM', '').replace('+', '').replace('%', '').replace(',', '.'))
            taxa_igpdi = float(1 + (TaxasInvestimentos.igpdi12 / 100)) * (1 + taxa/100)
            taxa_total = (taxa_igpdi - 1) * 100
            rentabilidade_bruta_dia = (taxa_igpdi ** (1/252) - 1) * 100
            taxaAntiga = "IGP-DI +{0}%".format(taxa)
            indice = 'IGP-DI'

        else:
            // +14,5%
            taxa = float(taxa.replace('+', '').replace('%', '').replace(',', '.').replace('A.A', ''))
            taxa_total = taxa
            fator_taxa = float(1 + (taxa / 100)) //  14.5 = 1.145
            rentabilidade_bruta_dia = (fator_taxa ** float(1/252) - 1) * 100 //  (1.145 ** (0.003968) - 1) * 100 = 0,0537
            taxaAntiga = "{0}%".format(taxa)
            indice = 'PRE'


        rentabilidade_bruta_periodo = ((float(1+ (rentabilidade_bruta_dia/100)) ** dias_uteis) - 1) * 100
        rentabilidade_bruta_mes = ((float(1 + (rentabilidade_bruta_periodo / 100)) ** float(21 / dias_uteis)) - 1) * 100
        rentabilidade_bruta_ano = ((float(1 + (rentabilidade_bruta_periodo / 100)) ** float(252 / dias_uteis)) - 1) * 100
        rentabilidade_liquida_periodo = (rentabilidade_bruta_periodo / 100) * (1 - ir) * 100
        rentabilidade_liquida_dia = ((float(1+(rentabilidade_liquida_periodo / 100)) ** float(1/dias_uteis)) - 1) * 100
        rentabilidade_liquida_mes = ((float(1 + (rentabilidade_liquida_periodo / 100)) ** float(21 / dias_uteis)) - 1) * 100
        rentabilidade_liquida_ano = ((float(1 + (rentabilidade_liquida_periodo / 100)) ** float(252 / dias_uteis)) - 1) * 100

        valor_minimo = float(valor_minimo)
        valor_bruto = valor_minimo * float(1 + (rentabilidade_bruta_periodo / 100))

        rentabilidade_poupanca_dia = (float(1+(TaxasInvestimentos.poup12/100)) ** float(1/252) - 1) * 100
        rentabilidade_poupanca_periodo = ((float(1+ (rentabilidade_poupanca_dia/100)) ** dias_uteis) - 1) * 100
        valor_poup = round(valor_minimo * float(1 + (rentabilidade_poupanca_periodo / 100)), 2)

        if ir != 0:
            valor_ir = float(valor_bruto - valor_minimo) * ir
            valor_liquido = valor_bruto - valor_ir
        else:
            valor_liquido = valor_bruto
            valor_ir = 0

        valor_rendimento_liquido = round(valor_liquido - valor_minimo, 2)
        perc_rendimento_liquido_total = round((valor_rendimento_liquido / valor_minimo) * 100, 2)

        eq = calcula_equivalecia(tipo, ir, taxa_total)

        return taxaAntiga, dias_uteis, dias_corridos, round(taxa_total, 2), round(rentabilidade_bruta_dia, 6), round(rentabilidade_bruta_periodo, 2), ir * 100, round(rentabilidade_liquida_periodo, 2), round(rentabilidade_liquida_dia, 6), round(rentabilidade_liquida_mes, 2), round(rentabilidade_liquida_ano, 2), round(valor_bruto, 2), round(valor_ir, 2), round(valor_liquido, 2), round(valor_rendimento_liquido, 2), perc_rendimento_liquido_total, taxa, indice, round(rentabilidade_poupanca_dia, 4), round(rentabilidade_poupanca_periodo, 2), valor_poup, eq, round(rentabilidade_bruta_mes, 4), round(rentabilidade_bruta_ano, 2)
	}


    get_taxas(self, dados_corretora) {
        valores = TaxasInvestimentos().verifica_taxa(dados_corretora['taxa'], dados_corretora['preco'], dados_corretora['vencimento'], dados_corretora['tipo'])
        taxa = valores[0]

        preco = float(dados_corretora['preco'])
        try:
            taxa = float(taxa)
            taxa = "{0}% PRE".format(taxa)
        except:
            pass

        dados_corretora['taxa'] = valores[0]
        dados_corretora["tir"] = valores[6]    //  taxa IR
        dados_corretora["vir"] = valores[12]   //  valor IR
        dados_corretora["dc"] = valores[2]     //  dias corridos
        dados_corretora["du"] = valores[1]     //  dias uteis
        dados_corretora["rbd"] = valores[4]    //  rentabilidade bruta ao dia
        dados_corretora["rbm"] = valores[22]   //  rentabilidade bruta ao mes
        dados_corretora["rba"] = valores[23]   //  rentabilidade bruta ao ano
        dados_corretora["rbp"] = valores[5]    //  rentabilidade bruta no periodo
        dados_corretora["rlp"] = valores[7]    //  rentabilidade liquida no periodo
        dados_corretora["rld"] = valores[8]    //  rentabilidade liquida ao dia
        dados_corretora["rlm"] = valores[9]    //  rentabilidade liquida ao mes
        dados_corretora["rla"] = valores[10]   //  rentabilidade liquida ao ano
        dados_corretora["vb"] = valores[11]    //  valor bruto
        dados_corretora["vl"] = valores[13]    //  valor liquido
        dados_corretora["vrl"] = valores[14]   //  valor rendimento liquido
        dados_corretora["prlt"] = valores[15]  //  percentual rendimento liquido total
        dados_corretora["juros"] = valores[16] //  juros (100%, 14%, etc)
        dados_corretora["idx"] = valores[17]   //  indice (CDI, IPCA, IGPM, PRE)
        dados_corretora["rpd"] = valores[18]   //  rentabilidade poupanca ao dia
        dados_corretora["rpp"] = valores[19]   //  rentabilidade poupanca no periodo
        dados_corretora["vpp"] = valores[20]   //  valor poupanca no periodo
        dados_corretora["tt"] = valores[3]     //  taxa total
        dados_corretora["vencimento"] = '{0} dias'.format(valores[2])
	}
}
*/