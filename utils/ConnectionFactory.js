"use strict";

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

function _dummy_() {};

class Database {
	constructor(name, connection) {
		if (!name) {
			throw "Database name must be a valid string";
		} else if (!connection) {
			throw "Connection must be a valid Mongo driver";
		}
		
		this.collections = {};
		this._name = name;
		this.connection = connection;
	}
	
	/* Get the database name */
	get name() {
		return this._name;
	}
	
	/* Proxy function */
	from(name) {
		return this.collection(name);
	}
	
	/* Returns the desired collection */
	collection(name) {
		// console.log("Looking for", name, "in", this.collections);
		if (name in this.collections) {
			return this.collections[name];
		} else {
			// add a new collection
			return this.generateCollection(name);
		}
	}
	
	/* For a given name, generates a new schemaless collection */
	generateCollection(name) {
		this.collections[name] = new Collection(this.connection, name);
		// console.log("Created", name);
		return this.collections[name];
	}
}

class Collection {
	constructor (connection, name) {
		this.connection = connection;
		this._name = name;
		
		this.schema = new mongoose.Schema({
			// ... none shall be defined here
		}, { collection: name, strict: false, autoIndex: false });
		this.model = this.connection.model(this._name, this.schema);
	}
	
	_copy (a, b) {
		for (let key in b) {
			a[key] = b[key];
		}
	}
	
	_copyb (a, b) {
		for (let i = 0; i < b.length; ++i) {
			a[i] = b[i];
		}
	}
	
	get name() {
		return this._name;
	}
	
	prepareQuery(query) {
		return query ? query : {_id: {"$exists": true}};
	}
	
	findOne (query, callback) {
		var model = {};
		this.model.findOne(this.prepareQuery(query), (err, result) => {
			if (err) {
				callback(err, null);
			}
			this._copy(model, result);
			callback(err, model);
		});
		return model;
	}
	
	find (query, callback) {
		var models = [];
		this.model.find(this.prepareQuery(query), (err, result) => {
			if (err) {
				callback(err, null);
			}
			this._copyb(models, result);
			callback(err, models);
		});
		return models;
	}
	
	insertOne(query, callback) {
		var model = new this.model(query);
		return model.save(query, callback);
	}
	
	insert(queries, callback) {
		var models = [];
		var err = null;
		for (var i = 0; i < queries.length; ++i) {
			var model = this.insertOne(queries[i], function (_err, result) {
				if (_err) {
					callback(_err, undefined);
				}
				models.push(model);
				if (i == queries.length - 1) {
					callback(null, models);
				}
			});
		}
	}
	
	getUpdateArgs(args) {
		var arg1, arg2;
		var out = {};
		
		if (args.length <= 2) {
			out.callback = _dummy_;
			out.upsert = false;
			return out;
		} else if (args.length == 3) {
			arg1 = args[2];
			arg2 = false;
		} else {
			arg1 = args[2];
			arg2 = args[3];
		}
		
		if (!arg1) {
			if (arg2 instanceof Function) {
				out.callback = arg2;
				out.upsert = false;
			} else {
				out.callback = _dummy_;
				out.upsert = new Boolean(arg2);
			}
		} else if (arg1 instanceof Function) {
			out.callback = arg1;
			out.upsert = new Boolean(arg2);
		} else {
			out.callback = arg2 instanceof Fucntion ? arg2 : _dummy_;
			out.upsert = new Boolean(arg1);
		}
		return out;
	}
	
	updateOne(match, query, args) {
		args = getUpdateArgs(arguments);
		return this.model.where(match).setOptions({multi: false, upsert: args.upsert}).update(query, args.callback);
	}
	
	update(match, query, args) {
		args = getUpdateArgs(arguments);
		return this.model.where(match).setOptions({multi: true, upsert: args.upsert}).update(query, args.callback);
	}
}

class ConnectionFactory {
	constructor() {
		this.connector = mongoose;
		this.dburl = process.env.OPENSHIFT_MONGODB_DB_URL;
		this.dbname = process.env.OPENSHIFT_APP_NAME;
		
		if (!this.dburl || !this.dbname) {
			this.dburl = "mongodb://localhost";
			this.dbname = "rendafixa";
		}
	}
	
    consructor(_name, _url) {
		this.connector = mongoose;
		this.dburl = process.env.OPENSHIFT_MONGODB_DB_URL;
		this.dbname = process.env.OPENSHIFT_APP_NAME;
		
		if (!this.dburl || !this.dbname) {
			this.dburl = _url ? _url : "mongodb://localhost";
			this.dbname = _name ? _name : "rendafixa";
		}
	}
	
    connect(_name, _url) {
		var dburl = _url ? _url : this.dburl;
		var dbname = _name ? _name : this.dbname;
		
		// console.log(dburl, dbname);
		if (dburl.lastIndexOf('/') != dburl.length - 1) {
			dburl += '/';
		}
		
		var url = dburl + dbname;
		console.log("Connected to", url);
		
		return new Database(dbname, this.connector.createConnection(url));
	}
}

module.exports = new ConnectionFactory();