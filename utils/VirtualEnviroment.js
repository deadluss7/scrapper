'use strict';

let os = require('os');
let fs = require('fs');
let path = require('path');

fs._rmdirRecursive = function (path) {
	fs.readdirSync(path).forEach( (file) => {
		var curPath = path + "/" + file;
		if(fs.lstatSync(curPath).isDirectory()) { // recurse
			this._rmdirRecursive(curPath);
		} else { // delete file
			fs.unlinkSync(curPath);
		}
	});
	fs.rmdirSync(path);
};

fs.rmdirRecursive = function(path) {
	setTimeout(() => {
		try {
			this._rmdirRecursive(path);
		} catch (err) {
			console.error(err);
		}
	}, 10);
};

class VirtualEnviroment {
	constructor () {
		this._temp_dir = path.resolve(os.tmpdir(), 'Sjedon');
		this._apps = {};
		this._handleGenerator = function* () {
			let handle = 0;
			while (1) {
				yield handle++;
			}
		}();
	}
	
	register (appHandle) {
		let app = this._apps[appHandle];
		if (!app) {
			app = {
				dir: path.resolve(this._temp_dir, appHandle),
				handles: {}
			};
			this._apps[appHandle] = app;
		}
		let handle = this._handleGenerator.next();
		let dir = path.resolve(app.dir, handle);
		let _handle = {
			dir: dir,
			app: app
		};
		app.handles[handle] = _handle;
		return _handle;
	}
	
	drop (handle) {
		fs.rmdirRecursive(handle.dir);
	}
	
	dropApp (appHandle) {
		fs.rmdirRecursive(this._apps[appHandle].dir);
	}
	
	get path() {
		return path;
	}
}

module.exports = new VirtualEnviroment();