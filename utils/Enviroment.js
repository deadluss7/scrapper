"use strict";

var http = require('http');
var https = require('https');

class Enviroment {
	constructor () {
		let args = process.args ? process.args : [];
		this._isProduction = args.length <= 2;
		if (this.isProduction()) {
			this._host = 'python-fwagner.rhcloud.com';
			this._protocol = 'http://';
		} else {
			this._host = 'localhost:5000';
			this._protocol = 'http://';
		}
	}
	
	isProduction() {
		return this._isProduction;
	}
	
	get protocol () {
		return this.protocol
	}
	
	get protocolLib () {
		return this._protocol.includes('https') ? https : http;
	}
	
	get hostname () {
		return this._host
	}
	
	get host () {
		return this._protocol + this._host;
	}
}

module.exports = new Enviroment();