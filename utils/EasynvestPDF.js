'use strict';
let PDFParser = require("pdf2json/PDFParser");
let pdfParser = new PDFParser();
let latin1 = require('./UTF-8Latin1');
let path = require('path');
let fs = require('fs');

/*
 * Overrides - BEGIN
 */

Object.prototype.emplace = function (field, defaultValue) {
	if (!this[field]) {
		this[field] = defaultValue;
	}
	return this[field];
};

function _onPDFJSParseDataReady (data) {
	if (!data) { //v1.1.2: data===null means end of parsed data
		// nodeUtil.p2jinfo("PDF parsing completed.");
		let output = {"formImage": this.data};
		this.emit("pdfParser_dataReady", output);
		if (typeof this.flushCallback === 'function') {
			this.push(output);
			this.flushCallback();
			this.flushCallback = null;
		}
	}
	else {
		Object.assign(this.data, data);
	}
}

function _onPDFJSParserDataError (data) {
	this.data = null;
	this.emit("pdfParser_dataError", {"parserError": data});
}

function _loadRawPDF(rawpdf, verbosity) {
	this.setVerbosity(verbosity);	//debug
	//nodeUtil.p2jinfo("about to load PDF file " + pdfFilePath);	//debug
	this.pdfFilePath = "localhost.pdf";	//Cant be null, so I set to anythings
	// Bypass the whole file loading! We already have it in memory!
	// Real processing starts here
	//_processPDFContent.apply(this, [undefined, rawpdf]);
	this.data = {};
	this.PDFJS.on("pdfjs_parseDataReady", _onPDFJSParseDataReady.bind(this));
	this.PDFJS.on("pdfjs_parseDataError", _onPDFJSParserDataError.bind(this));
	this.PDFJS.parsePDFData(rawpdf);
}

/*
 * Overrides - END
 */

var header_map = {
	"Emissor": "emissor",
	"Data de Liquidação": "data",
	"Título": "tipo",
	"Indexador": "idx",
	"% do indexador": "taxa",
	"Vencimento": "datavencto",
	"Quantidade / Valor Nominal": "qtd",
	"Preço Unitario da Operação": "pu",
	"CNPJ / CPF": "cpf"
};

function round (n) {
	return parseInt(n);
}

function dX(x1, x2) {
	let n = (x1.x - x2.x);
	return  n < 0 ? -n : n;
}

function dY(y1, y2) {
	let n = (y1.y - y2.y);
	return  n < 0 ? -n : n;
}

function getPoint(obj, _plain) {
	if (_plain) {
		return {x: obj.x, y: obj.y};
	} else {
		return {x: round(obj.x), y: obj.y};
	}
}

function str2ab(str) {
	var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
	var bufView = new Uint16Array(buf);
	for (var i=0, strLen=str.length; i<strLen; i++) {
		bufView[i] = str.charCodeAt(i);
	}
	return bufView;
}


class EasynvestPDF {
	constructor () {
		
	}
	
	updateDeepSearchDict (_dict, _data) {
		_dict = _dict ? _dict : {};
		_dict.emplace(_data.valinv, {});
		_dict.emplace(_data.tipo, {});
		_dict.emplace(_data.idx, {});
		_dict.emplace(_data.datavencto, {});
		_dict[_data.taxa] = {
			data: _data.data,
			nota: _data.nota
		};
	}
	
	parse (json) {
		let values = {};
		let placeholders = {};
		let fields = {};
		let texts = json.formImage.Pages[0].Texts;
		for (let i = 0; i < texts.length; ++i) {
			let text = texts[i];
			let value = latin1.parseText(text.R[0].T);
			let coord = text.x.toString() + ";" + text.y.toString();
			let header = header_map[value];
			if (header) {
				placeholders[coord] = {header: header, x: text.x, y: text.y};
			} else {
				values[coord] = {text: value, x: text.x, y: text.y};
			}
		}
		
		for (let key in placeholders) {
			let placeholder = placeholders[key];
			let value = this.findClosest(placeholder, values);
			// console.log("ON", key, "Got", value.text, "for", placeholder.header);
			fields[placeholder.header] = value;
		}
		
		let _dict = {};
		this.updateDeepSearchDict(_dict, fields);
		return _dict;
	}
	
	findClosest (placeholder, values) {
		let closest;
		let curDistance;
		let target = getPoint(placeholder, true);
		
		for (let key in values) {
			let value = values[key];
			let point = getPoint(value);
			let dx = dX(point, target);
			let dy = dY(point, target);
			if (closest === undefined) {
				closest = value;
				curDistance = {
					x: dx,
					y: dy
				};
			} else {
				if (dx <= curDistance.x) {
					if (dy <= curDistance.y) {
						closest = value;
						curDistance = {
							x: dx,
							y: dy
						};
					}
				}
			}
		}
		
		return closest;
	}
	
	load (pdf, isRaw) {
		return new Promise((resolve, reject) => {
			pdfParser.on("pdfParser_dataError", errData => {
				reject(errData.data);
			});
			pdfParser.on("pdfParser_dataReady", pdfData => {
				// console.log("READY:\n", pdfData.formImage.Pages[0].Texts);
				let fields = this.parse(pdfData);
				resolve(fields);
			});
			if (isRaw) {
				_loadRawPDF.apply(pdfParser, [pdf]);
			} else {
				pdfParser.loadPDF(pdf);
			}
		});
	}
	
	get (pdf, isPersistent) {
		pdf.buffer = new Buffer(str2ab(pdf.buffer));
		
		/*Saves the file first then pdf2json will parse from it*/
		if (isPersistent) {
			let filename = pdf.nota + ".pdf";
			pdf.path = path.resolve(pdf.path, filename);
			console.log(filename, pdf.buffer.length);
			return new Promise((resolve, reject) => {
				fs.writeFile(filename, pdf.buffer, (err) => {
					if (err) {
						reject(err);
					}
					// Discard the buffer
					pdf.buffer = null;
					this.load(pdf.path, false).then((result) => {
						resolve(result);
					}).catch((err) => {
						reject(err);
					});
				});
			});
		} else {
			/* pdf2json will read from the buffer */
			return this.load(pdf.buffer, true);
		}
		
	}
	
	deepSearch(_data, _dict) {
		let tmp = _dict[_data.valinv];
		if (tmp !== undefined) {
			tmp = tmp[_data.tipo];
			if (tmp !== undefined) {
				tmp = tmp[_data.idx];
				if (tmp !== undefined) {
					tmp = tmp[_data.datavencto];
					if (tmp !== undefined) {
						tmp = tmp[_data.taxa];
						if (tmp !== undefined) {
							_data.data = tmp.data;
							_data.nota = tmp.nota;
						}
					}
				}
			}
		}
	}
	
	crossInformations (investments, table) {
		for (let i = 0; i < investments.length; ++i) {
			if (investments[i].nota) {
				continue;
			}
			this.deepSearch(investments[i], table);
		}
	}
}

module.exports = new EasynvestPDF();