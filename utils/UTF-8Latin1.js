'use strict';

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

var code_map = {
	"%20":		" ",
	"%24":		"$",
	// "%25":	"%",
	"%2C":		",",
	"%2F":		"/",
	"%3A":		":",
	"%40":		"@",
	"%C2%BA":	"º",
	"%C3%81":	"Á",
	"%C3%83":	"Ã",
	"%C3%8D":	"Í",
	"%C3%93":	"Ó",
	"%C3%87":	"Ç",
	"%C3%89":	"É",
	"%C3%95":	"Õ",
	"%C3%9A":	"Ú",
	"%C3%A3":	"ã",
	"%C3%A7":	"ç",
	"%C3%A1":	"á",
	"%C3%AD":	"í",
	"%C3%B3":	"ó",
	"%C3%BA":	"ú"
}

function parseText(text) {
	let _text = text.toString();
	for (let key in code_map) {
		_text = _text.replaceAll(key, code_map[key]);
	}
	return _text.replace('%25', '%');
}

module.exports = {
	parseText: parseText
};