class LinkedNode {
	constructor () {
		this.next = null;
		this.prev = null;
		this.value = undefined;
	}
	
	constructor (node) {
		this.next = node;
		node.prev = this;
		this.prev = null;
		this.value = undefined;
	}
	
	constructor (node, value) {
		this.next = node;
		node.prev = this;
		this.prev = null;
		this.value = value;
	}
	
	get left () {
		return this.prev;
	}
	
	get right () {
		return this.next;
	}
}

module.exports = {
	LinkedNode: LinkedNode,
};