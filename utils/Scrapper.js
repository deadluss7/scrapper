"use strict";
process.setMaxListeners(0);

/* Third Party */
var Nightmare = require('nightmare');
var vo = require('vo');

/* Nightmare upgrade */ 
function waitClick(waitSelector, clickSelector) {
	clickSelector = !clickSelector ? waitSelector : clickSelector;
	return this.wait(waitSelector).click(clickSelector);
}

function clickWait(clickSelector, waitSelector) {
	waitSelector = !waitSelector ? 100 : waitSelector;
	return this.click(clickSelector).wait(waitSelector);
}

function evaluate(selector, property) {
	// console.log("GET::" + selector + "::" + property);
	return document.querySelector(selector)[property];
}

function get(selector, property, wait) {
	if (wait) {
		return this.evaluate(evaluate, selector, property).wait(selector);
	} else {
		return this.evaluate(evaluate, selector, property);
	}
}

function getText(selector, wait) {
	return this.get(selector, "innerText", wait);
}

function getValue(selector, wait) {
	return this.get(selector, "value", wait);
}

function _dummy_(_arg) {return _arg;}

class Scrapper {
	constructor () {
		this.processCount = 0;
		this._was_inited = false;
		this._master = undefined;
		this.log = _dummy_;
		this.time = _dummy_;
		this.error = console.error;
	}
	
	validateArgs(args, expected, callback) {
		if (args.length >= expected) {
			callback();
		} else {
			this.throw("Args do not meet expected length of " + expected);
		}
	}
	
	init(enableDebug, options) {
		options = options ? options : {
			show: enableDebug
		};
		
		this.nightmare = new Nightmare(options);
		this.nightmare.waitClick = waitClick;
		this.nightmare.clickWait = clickWait;
		this.nightmare.get = get;
		this.nightmare.getText = getText;
		this.nightmare.getValue = getValue;
		if (enableDebug) {
			console.time("debug");
			this.log = console.log;
			this.time = console.timeEnd;
			this.error = console.log;
		}
		this._was_inited = true;
		if (!this._master) {
			return this.slave(options.slave);
		} else {
			return this;
		}
	}
	
	initSlave(enableDebug, options) {
		options = options ? options : {
			show: enableDebug
		};
		options.slave = true;
		return this.init(enableDebug, options);
	}
	
	slave (bool) {
		this._master = bool ? process : {send: _dummy_};
		return this;
	}
	
	out (data, type, prefix, suffix) {
		try {
			this._out_unsafe(JSON.stringify(data ? data : {}), type, prefix, suffix);
		} catch (err) {
			console.trace();
			this.throw(err);
		}
		return this;
	}
	
	_out_unsafe (data, type, prefix, suffix) {
		type = type ? type.toString() : "stdout";
		data = (prefix ? prefix : "") + data + (suffix ? suffix : "");
		this._master.send(type + ";" + data);
	}
	
	outArray (data, type) {
		if (type === 'end') {
			this.out(data, type);
		} else if (!data.length) {
			this._out_unsafe("[]", type);
		} else {
			this._out_unsafe("[", type);
			this.out(data[0], type);
			for (let i = 1; i < data.length; ++i) {
				this.out(data[i], type, ',', "");
			}
			this._out_unsafe("]", type);
		}
	}
	
	get Inited () {
		return this._was_inited;
	}
	
	setOnEnd (kill) {
		this.kill = kill instanceof Function ? kill : _dummy_;
		return this;
	}
	
	setOnError(error) {
		this.catch = error instanceof Function ? error : _dummy_;
		return this;
	}
	
	start(generator) {
		if (!this.Inited) {
			this.init();
		}
		return new Promise((resolve, reject) => {
			try {
				vo(generator)((err, result) => {
					if (err) {
						console.log('Reject', err);
						reject(err);
					} else {
						console.log('Resolve');
						resolve(result);
					}
				});
			} catch (err) {
				console.log('Reject', err);
				reject(err);
			}
		});
	}
	
	createSubTask(callback) {
		if (!arguments) {
			throw "Scrapper::crateTask requires at least the callback to be called";
		}
		var scope = [].shift.call(arguments);
		var args = arguments;
		this.processCount += 1;
		setTimeout(function () {
			callback.apply(scope, args);
		}, 100);
		return this;
	}
	
	createTimedSubTask(callback, time, scope, args) {
		if (!callback) {
			throw "Scrapper::crateTask requires at least the callback to be called";
		}
		this.processCount += 1;
		setTimeout(function () {
			callback.apply(scope ? scope : callback, Array.isArray(args) ? args : []);
		}, time);
		return this;
	}
	
	catch(error, safe) {
		this.error(error);
		this.out(error, "stderr");
		this.end(!safe);	//safe: false -> Abort
	}
	
	kill() {
		this.out("ok!", "end");
		process.exit();
	}
	
	end(forced) {
		if (forced || this.processCount <= 0) {
			this.time("debug");
			this.kill.apply(this);
		} else {
			this.processCount -= 1;
		}
	}
	
	throw(error, safe) {
		this.catch.apply(this, [error, safe]);
	}
	
	assert(cond, error, safe) {
		if (!cond) {
			this.throw(error, safe);
		}
	}
	
	get Nightmare() {
		return this.nightmare;
	}
	
	countRunningTasks() {
		return this.processCount;
	}
	
	remap(input, mapping, transform) {
		var output = {};
		if (transform) {
			for (let key in input) {
				let value = input[key];
				if (key in mapping) {
					output[mapping[key]] = transform[key] ? transform[key](value) : value;
				}
			}
		} else {
			for (let key in input) {
				let value = input[key];
				if (key in mapping) {
					output[mapping[key]] = value;
				}
			}
		}
		return output;
	}
	
//	scrap (args) {
//			return vo(function* () {
//				if (arguments.length < 1) {
//					this.throw("Can't evalaute without a callback");
//				}
//				return yield nightmare.evaluate.apply(arguments, nightmare);
//			}();
//		}
	
//		scrapJson (args) {
//			return JSON.parse(this.scrap.apply(arguments, this));
//		}
}

module.exports = new Scrapper();