"use strict";

var ConnectionFactory = require('./ConnectionFactory');

var _nr = {
	'Fitch': {
		'AAA': 25, 'AA+': 24, 'AA': 23, 'AA-': 22, 'A+': 21, 'A': 20, 'A-': 19, 'BBB+': 18, 'BBB': 17, 'BBB-': 16, 'BB+': 15, 'BB': 14, 'BB-': 13, 'B+': 12, 'B': 11, 'B-': 10, 'CCC+': 9, 'CCC': 8, 'CCC-': 7, 'CC+': 6, 'CC': 5, 'CC-': 4, 'DDD': 3
	},
	'Austin': {
		'AAA': 25, 'AA+': 24, 'AA': 23, 'AA-': 22, 'A+': 21, 'A': 20, 'A-': 19, 'BBB+': 18, 'BBB': 17, 'BBB-': 16, 'BB+': 15, 'BB': 14, 'BB-': 13, 'B+': 12, 'B': 11, 'B-': 10, 'CCC+': 9, 'CCC': 8, 'CCC-': 7, 'CC+': 6, 'CC': 5, 'CC-': 4, 'DDD': 3
	},
	'S&P': {
		'AAA': 25, 'AA+': 24, 'AA': 23, 'AA-': 22, 'A+': 21, 'A': 20, 'A-': 19, 'BBB+': 18, 'BBB': 17, 'BBB-': 16, 'BB+': 15, 'BB': 14, 'BB-': 13, 'B+': 12, 'B': 11, 'B-': 10, 'CCC+': 9, 'CCC': 8, 'CCC-': 7, 'CC': 6, 'C': 5, 'D': 3
	},
	'Moodys': {
		'Aaa': 25, 'Aa1': 24, 'Aa2': 23, 'Aa3': 22, 'A1': 21, 'A2': 20, 'A3': 19, 'Baa1': 18, 'Baa2': 17, 'Baa3': 16, 'Ba1': 15, 'Ba2': 14, 'Ba3': 13, 'B1': 12, 'B2': 11, 'B3': 10, 'Caa1': 9, 'Caa2': 8, 'Caa3': 7, 'Ca': 6, 'D': 3
	},
	'': {'': 0}
};

class Emissores {
	constructor() {
		this.db = ConnectionFactory.connect();
		this.emissores = [];
	}
	
	fetch (onsuccess, onerror) {
		console.log("Emissores::Fetching");
		let collection = this.db.from('emissores');
		var self = this;
		collection.find({_id: {"$exists": true}}, function (err, documents) {
			if (err || !documents) {
				if (onerror) onerror();
			}
			if (onsuccess) onsuccess(documents);
			self.emissores = documents;
		});
	}
	
	isValid() {
		return this.emissores ? this.emissores.length : false;
	}
	
	emissor(emissor) {
		emissor = emissor.toString().toUpperCase();
		for (let i = 0; i < this.emissores; ++i) {
			let item = this.emissores[i];
			if (item.palavras_chaves.includes(item)) {
				return {
					'rating' : item["ratings"][0]["rating"], 
					'agencia' : item["ratings"][0]["agencia"], 
					'nome' : item["nome_amigavel"], 
					'encontrado' : true, 
					'nr': _nr[item["ratings"][0]["agencia"]][item["ratings"][0]["rating"]]
				};
			}
		}
		return {
			'rating' : '',
			'agencia' : '',
			'nome' : emissor,
			'encontrado' : false, 
			'nr' : 0
		};
	}
}

module.exports = new Emissores();