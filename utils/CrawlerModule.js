'use strict';
const spawner = require('child_process').fork;

class CrawlerModule {
	constructor() {
		this._onsucc = function (data) {return data;};
		this._onerr = function (data) {return data;};
		this.env = require('./VirtualEnviroment');
	}
	
	set onError(value) {
		this._onerr = value;
	}
	
	get onError() {
		return this._onerr;
	}
	
	set onSuccess(value) {
		this._onsucc = value;
	}
	
	get onSuccess() {
		return this._onsucc;
	}
	
	// Spawn the new instance
	require () {
		let args = Array.prototype.slice.call(arguments);
		let handle = args.shift();
		
		// Assert the handle is valid
		handle = !handle ? {} : handle;
		let name = handle.name;
		if (!name) {
			throw "Cant require undefined";
		} else if (!handle.dir) {
			throw 'Cant start withou an virtual enviroment';
		}
		
		// Prepare the module name for spawn
		name = this.env.path.resolve('robo', name + ".js");
		// Push the virtual directory back to the list
		args = [handle.dir].concat(args);
		
		// Spawn the new process
		console.log("Run:", name, args);
		// return spawner(name, args, {stdio: [process.stdin, process.stdout, process.stderr, 'pipe', 'pipe']});
		return spawner(name, args, {stdio: [process.stdin, process.stdout, process.stderr, 'ipc']});
	}
	
	// Destroy the handle
	beforeResolution (handle) {
		// drop the handle
		// this.env.drop(handle)
	}
	
	// Run a new instance and return its promise
	run () {
		// assert there is at least a parameter
		if (arguments.length < 1) {
			throw "CrawlerModule::run requires at least a module name to invoke";
		}
		
		var proc = {
			data: "",
			err: "",
			child: undefined
		};
		return new Promise((resolve, reject) => {
			// Prepare handle aka virtual enviroment for the instance
			console.log("Getting new handle for the process:");
			let name = arguments[0];
			var handle = this.env.register(name);
			handle.name = name;
			arguments[0] = handle;
			console.log(handle);
			
			// Spawn the new instance
			proc.child = this.require.apply(this, arguments);
			
			// Parse any sent message
			proc.child.on('message', (m) => {	// (returned_code) => {
				let ev = m.substring(0, m.search(';'));
				let data = m.substring(m.search(';')+1);
				if (ev === 'end') {		//kill the child since it wont do it by itself
					console.log('Done!');
					proc.child.kill();
					
					// Verify the process state for errors
					if (proc.err) {
						this.beforeResolution(handle);
						reject(this.onError(proc.err));
					} else {
						this.beforeResolution(handle);
						resolve(this.onSuccess(proc.data));
					}
				} else if (ev === 'stdout') {	//any message
					console.log("DATA");
					proc.data += data;
				} else if (ev === 'stderr') {	//error message
					console.log("Error", data);
					proc.err += data;
				}
			});
			// Awaiting for the process to be resolved or rejected
			console.log('Now awaiting for resolution\n\n');
		});
	}
}

module.exports = new CrawlerModule();