'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var CrawlerModule = require('./utils/CrawlerModule');

var app = express();

CrawlerModule.onSuccess = function (output) {
	return {
		json: JSON.parse(output),
		status: 200
	};
};

CrawlerModule.onError = function (err) {
	return {
		json: "{\"error\": \"" + err.replace('"', '\\"') + "\"}",
		status: 400
	};
};

app.use(bodyParser.json());

app.get('/', function(req, res) {
	res.send('<h1>Test</h1>');
});

app.get('/crawler/:name/:acc/:pass', function(req, res) {
	let name = req.params.name;
	let acc = req.params.acc;
	let pass = req.params.pass;
	
	if (!name || !acc || !pass) {
		return res.status(401).send('{"error": "Wrong arguments"}');
	}
	console.log(name, acc, pass);
	CrawlerModule.run(name, acc, pass).then((data) => {
		console.log('Resolved');
		res.status(data.status).send(data.json);
	}).catch((data) => {
		console.log('Rejected', data);
		res.status(data.status).send(data.json);
	});
});

app.post('/crawler', function(req, res) {
	let json = req.body;
	let name = json.name;
	let acc = json.acc;
	let pass = json.pass;
	
	if (!name || !acc || !pass) {
		return res.status(401).send('{"error": "Wrong arguments"}');
	}
	
	console.log(name, acc, pass);
	CrawlerModule.run(name, acc, pass).then((data) => {
		console.log('Resolved');
		res.status(data.status).send(data.json);
	}).catch((data) => {
		console.log('Rejected', data);
		res.status(data.status).send(data.json);
	});
});

app.listen(5000, function () {
	console.log('Starting');
});
